<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<   ![endif]-->

    <head>

        <!-- Default character encoding -->
        <meta charset="utf-8">
        
        <!-- Forces IE to NOT use compatability mode -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Page Title -->
        <title>
            Website &middot; Page
        </title>

        <!-- Display website at device width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Prefetch DNS for Contend Delivery Network (CDN) -->
        <link rel="dns-prefetch" href="//cdnjs.cloudflare.com" /> 

        <!-- Load the main stylesheets -->
        <link rel="stylesheet" href="../../dist/css/main.min.css">

        <!-- SEO Tags -->

            <!-- SEO Author -->
            <meta name="author" content="AUTHOR">

            <!-- SEO Description -->
            <meta name="description" content="DESCRIPTION">

            <!-- SEO Keywords -->
            <meta name="keywords" content="KEYWORDS, SEPARATED, BY, COMMAS">
            
        <!-- /SEO Tags-->

        <!-- Favicon's -->

            <!-- Apple Touch Icons -->
            <link rel="apple-touch-icon" sizes="57x57" href="../../favicons/apple-touch-icon-57x57.png">
            <link rel="apple-touch-icon" sizes="114x114" href="../../favicons/apple-touch-icon-114x114.png">
            <link rel="apple-touch-icon" sizes="72x72" href="../../favicons/apple-touch-icon-72x72.png">
            <link rel="apple-touch-icon" sizes="144x144" href="../../favicons/apple-touch-icon-144x144.png">
            <link rel="apple-touch-icon" sizes="60x60" href="../../favicons/apple-touch-icon-60x60.png">
            <link rel="apple-touch-icon" sizes="120x120" href="../../favicons/apple-touch-icon-120x120.png">
            <link rel="apple-touch-icon" sizes="76x76" href="../../favicons/apple-touch-icon-76x76.png">
            <link rel="apple-touch-icon" sizes="152x152" href="../../favicons/apple-touch-icon-152x152.png">
            <link rel="apple-touch-icon" sizes="180x180" href="../../favicons/apple-touch-icon-180x180.png">
            <!-- /Apple Touch Icons -->

            <!-- Favicons -->
            <link rel="icon" type="image/png" href="../../favicons/favicon-192x192.png" sizes="192x192">
            <link rel="icon" type="image/png" href="../../favicons/favicon-160x160.png" sizes="160x160">
            <link rel="icon" type="image/png" href="../../favicons/favicon-96x96.png" sizes="96x96">
            <link rel="icon" type="image/png" href="../../favicons/favicon-16x16.png" sizes="16x16">
            <link rel="icon" type="image/png" href="../../favicons/favicon-32x32.png" sizes="32x32">
            <!-- /Favicons -->

            <!-- Windows 8 Tile -->
            <meta name="msapplication-TileColor" content="#00aba9">
            <meta name="msapplication-TileImage" content="../../favicons/mstile-144x144.png">
            <!-- /Windows 8 Tile -->

        <!-- /Favicon's -->

    </head>