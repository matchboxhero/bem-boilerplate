<!-- /page-container -->

        <!--load scripts-->

            <!-- load modernizr from CDN -->
            <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.min.js"></script>

            <!-- load main js file -->
            <script src="../../dist/js/main.js"></script>

        <!--/load scripts-->

    </body>

</html>