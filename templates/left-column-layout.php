<!-- Pull page head from template partials -->
<?php include 'partials/head.php'; ?>

<!-- Assign Page Layout via class on the body tag -->
<body class="left-column-layout">

<!-- Pull Header from template partials -->
<?php include 'partials/header-container.php';?>

<!-- page-container -->
<div class="page-container">

    <!-- main-container -->
    <main class="main-container" role="main">

        MAIN CONTENT

    </main> 
    <!-- /main-container -->

    <!-- Pull Left Column from template partials -->
    <?php include 'partials/left-container.php';?>

    <!-- Pull footer-container from template partials -->
    <?php include 'partials/footer-container.php';?>

</div>

<!-- Pull Page Foot from template partials -->
<?php include 'partials/foot.php';?>