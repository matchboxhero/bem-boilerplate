// Require all plugins and dependencies

var gulp         = require('gulp'),
    jshint       = require('gulp-jshint'),
    sass         = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss    = require('gulp-minify-css'),
    imagemin     = require('gulp-imagemin'),
    rename       = require('gulp-rename'),
    clean        = require('gulp-clean'),
    newer        = require('gulp-newer'),
    connect      = require('gulp-connect'),
    gulpIgnore   = require('gulp-ignore'),
    browserSync  = require('browser-sync'),
    reload       = browserSync.reload,
    imageThumbs  = '**/images/**/Thumbs.db';

// Lint Task
gulp.task('lint', function() {
    return gulp.src('src/scripts/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(reload({stream: true}));
});

// Compile and Concatenate All Sass Files and Output into dist Directory

gulp.task('styles', function() {
  return gulp.src('src/styles/main.scss')
    .pipe(sass({ style: 'expanded' }))
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(minifycss())
    .pipe(gulp.dest('dist/css'))
    .pipe(reload({stream: true}));
});

gulp.task('scripts', function() {
  return gulp.src('src/scripts/**/*.js')
    .pipe(gulp.dest('dist/js'));
});

// Clean all dist Directories Before Rebuild

gulp.task('clean', function() {
  return gulp.src(['dist/css'], {read: false})
    .pipe(clean());
});

// Compress All Image Files and Output into dist Directory

gulp.task('images', function() {
  return gulp.src('src/images/**/*')
    .pipe(gulpIgnore.exclude(imageThumbs))
    .pipe(newer('dist/images'))
    .pipe(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true }))
    .pipe(gulp.dest('dist/images'))
    .pipe(reload({stream: true, once: true}));
});

// Copy fonts to Dist

gulp.task('fonts', function() {
  return gulp.src('src/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'));
});

// Default Task

gulp.task('default', ['clean','scripts','images','fonts', 'lint','watch','webserver','browser-sync'], function() {
  gulp.start('styles');
  gulp.start('scripts');
});

// Watch Tasks  

gulp.task('watch', ['browser-sync'], function() {

  // Watch html files
  gulp.watch('*.html', reload);
  // Watch .js files
  gulp.watch('src/scripts/**/*.js', ['lint', 'scripts']);
  // Watch .scss Files
  gulp.watch('src/styles/**/*', ['styles']);
  // Watch images
  gulp.watch('src/images/**/*', ['images']);
  // Watch fonts
  gulp.watch('src/fonts/**/*', ['fonts']);

});

//Web Server 
gulp.task('webserver', function() {
  connect.server();
});

//Browser Sync
gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: "./"
        },
        notify: false
    });
});