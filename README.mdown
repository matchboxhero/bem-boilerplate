## BEM Boilerplate

The BEM Boilerplate has been written to use as a starting point for new projects, it includes a mostly empty directory and file structure which will be fitting for most projects. For the time being the BEM Boilerplate has a static HTML file for templating this is the easiest way to create static HTML projects.

### Gulp

The BEM Boilerplate uses gulp to handle most of the heavy lifting and automate much of the workflow; 

To get started using the gulp file open terminal, navigate to the project directory and type 'npm install'. Running this command will install all of the dependencies required by the project.

There are three gulp files included in the project; 'gulpfile.js' one named 'gulpfile-advanced.js' and another 'gulpfile-basic.js', by default 'gulpfile.js' is using the configuration from 'gulpfile-basic.js', both are included so you can replace the default gulpfile and still have a copy of the basic configuration.

The basic gulp file included in the project will; lint JS. Auto-prefix, concatonate and minify CSS. Compress and optimise images. Create a dist folder for the final outputted project containing all of the optimised and minified files. 

The advanced gulp file will do all of the above and set up a local server (which is viewable from any device) which synchronises scrolling and clicks with browser reload and will refresh the page upon new files being added or code being compiled. 

The gulpfile included in the project ('gulpfile.js') is using the gulpfile-basic.js code if you wish to use the advanced gulp file then you can replace the contents of 'gulpfile.js' with the contents of 'gulpfile-advanced.js'.

The majority of the project setup has already been done so you can simply start developing.

### Templates

The BEM boilerplate also has a templates foler which contains three different layouts; single column, two column and three column. These templates are made up of partials for each section of the layout. More information can be found in the 'README.mdown' file in the templates folder.

### Dependencies

BEM Boilderplate includes a gulpfile which will do much of the compiling and compressing needed, to use gulp you will need to install node and node package manager, then gulp. All of the gulp module dependencies can be installed by navigating to the project folder in terminal and using the command 'npm install' (if you see a permissions error use 'sudo npm install' and enter your password when prompted). This will install all of the required modules from node package manager.

The BEM Boilerplate is built upon the Viaduct CSS framework, this can be updated or downloaded using Bower. For more information about Viaduct please visit; https://github.com/ViaductCSS/Viaduct.

### Favicons

The favicons included in the boilerplate were genereated using; http://realfavicongenerator.net/. Although not all are absolutely necessary it is best practice to include them all to cover the widest range of devices possible.

### Installation

To use BEM Boilerplate, you can simply clone this repository.